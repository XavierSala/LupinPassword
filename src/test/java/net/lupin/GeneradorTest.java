package net.lupin;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class GeneradorTest {

	Generador generador;
	
	@Before
	public void setUp() throws Exception {
		generador = new Generador();
	}

	@Test
	public void testQueDonaErrorSiNoHiHaUsuari() {
		String expected = Generador.ERROR;
		String actual = generador.mostra("");
		assertEquals(expected, actual);
		
		actual = generador.mostra(null);
		assertEquals(expected, actual);
		
	}
	
	@Test
	public void testQueDonaElGenerador() {
		String expected = "M64d2Sooqk";
		String actual = generador.mostra("Xavier");
		assertEquals(expected, actual);
	}
	
	@Test
	public void testQueLesMaquinesFuncionen() {
		String expected = "4feb2KEGuN";
		String actual = generador.mostra("maquina/usuari");
		assertEquals(expected, actual);
	}

}
