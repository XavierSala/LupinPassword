package net.lupin;

/**
 * Calculate password
 *
 */
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Rectangle;
import javafx.fxml.FXMLLoader;


public class App extends Application {
	private static final int ALTURA = 250;
	private static final int AMPLADA = 441;

	@Override
	public void start(Stage primaryStage) {
		try {
			Parent root = (Parent)FXMLLoader.load(getClass().getClassLoader().getResource("login.fxml"));
			
			Scene scene = new Scene(root);
			primaryStage.setScene(scene);
			primaryStage.setMaxHeight(ALTURA);
			primaryStage.setMaxWidth(AMPLADA);
			primaryStage.setMinHeight(ALTURA);
			primaryStage.setMinWidth(AMPLADA);
			
			grafics(scene, (Pane)root);
			
			primaryStage.show();
			
		} catch(Exception e) {
			mostraError();
		}
	}
	
	private void grafics(Scene scene, Pane pane) {

		Rectangle colors = new Rectangle(scene.getWidth(), scene.getHeight(),
			     new LinearGradient(0f, 1f, 1f, 0f, true, CycleMethod.NO_CYCLE, new 
			         Stop[]{
			            new Stop(0, Color.web("#f8bd55")),
			            new Stop(0.14, Color.web("#c0fe56")),
			            new Stop(0.28, Color.web("#5dfbc1")),
			            new Stop(0.43, Color.web("#64c2f8")),
			            new Stop(0.57, Color.web("#be4af7")),
			            new Stop(0.71, Color.web("#ed5fc2")),
			            new Stop(0.85, Color.web("#ef504c")),
			            new Stop(1, Color.web("#f2660f")),}));
			colors.widthProperty().bind(scene.widthProperty());
			colors.heightProperty().bind(scene.heightProperty());
			pane.getChildren().add(0, colors); 		
	}

	private static void mostraError() {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Error");
		alert.setHeaderText(null);
		alert.setContentText("Em temo que el programa no funciona!");

		alert.show();
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
