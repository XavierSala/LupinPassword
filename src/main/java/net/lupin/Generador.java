package net.lupin;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;

public class Generador {

	private static final int NUMCARACTERS = 10;

	private static final String BASE = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabsdefghijklmnopqrstuvwxyz";
	private static final String MACHINESEPARATOR = ".+\\/[or|and].+";

	public static final String ERROR = "Error";

	public String mostra(String usuari) {
		if (usuari != null && !usuari.isEmpty()) {
			if (usuari.matches(MACHINESEPARATOR)) {
				try {
					return generaMachine(usuari).substring(0, 5) + generaCodi(usuari).substring(0, 5);
				} catch (NoSuchAlgorithmException e) {
					return ERROR;
				}
			}
			else {
				return ERROR;
			}		
		}

		return ERROR;
	}

	private String generaMachine(String usuari) throws NoSuchAlgorithmException {
		MessageDigest objSHA512 = MessageDigest.getInstance("SHA-512");
		byte[] bytSHA512 = objSHA512.digest(usuari.getBytes());
		BigInteger intNumSHA512 = new BigInteger(1, bytSHA512);
		String resultat = intNumSHA512.toString(16);
		while (resultat.length() < 128) {
			resultat = "0" + resultat;
		}
		return resultat;
	}

	private String generaCodi(String usuari) {

		StringBuilder resultat = new StringBuilder();

		for (int i = 0; i < NUMCARACTERS; i++) {
			String[] separats = separaSerial(i + 1, usuari);

			int codi = 0;
			int hash = nthPrime(i);
			for (String separat : separats) {

				for (char c : separat.toCharArray()) {
					hash = hash * nthPrime(30 + i) + c;
				}
				codi = (codi + hash) % BASE.length();
			}
			if (codi < 0) {
				codi = Math.abs(codi);
			}
			resultat = resultat.append(BASE.charAt(codi));
		}

		return resultat.toString();
	}

	private String[] separaSerial(int xifres, String contrasenya) {
		return Iterables.toArray(Splitter.fixedLength(xifres).split(contrasenya), String.class);
	}

	public static int nthPrime(int n) {
		int candidate, count;
		for (candidate = 2, count = 0; count < n; ++candidate) {
			if (isPrime(candidate)) {
				++count;
			}
		}
		return candidate - 1;
	}

	private static boolean isPrime(long n) {
		if (n < 2)
			return false;

		for (long i = 2; i * i <= n; i++) {
			if (n % i == 0)
				return false;
		}
		return true;
	}

}
