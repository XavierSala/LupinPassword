package net.lupin.controllers;

import javafx.fxml.FXML;

import java.util.Random;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.util.Duration;
import net.lupin.Generador;
import net.lupin.StringUtil;

/**
 * Controller for calculate logins
 * 
 * @author xavier
 *
 */
public class LoginController {

	@FXML
	private Pane pane;
	
	@FXML
	private Label calcula;

	@FXML
	private TextField usuari;
	
	private Timeline timeline;
	private Integer timeSeconds;
	private Random aleatori;
	private Generador generador;

	public void initialize() {
		aleatori = new Random();
		generador = new Generador();					
	}

	// Event Listener on Button.onAction
	@FXML
	public void generaContrasenya(ActionEvent event) {
		if (timeline != null) {
            timeline.stop();
        }
        calcula.setText("");
        timeSeconds = 50 + aleatori.nextInt(100);
 
        timeline = new Timeline(new KeyFrame(
        		Duration.millis(100),
        		ae -> {
        			calcula.setText(StringUtil.randomString(25));
        			timeSeconds --;
        			if (timeSeconds <= 0) {        				
        				timeline.stop();
        				String resultat = generador.mostra(usuari.getText());
        				calcula.setText(generador.mostra(usuari.getText()));
        				if (resultat.equals(Generador.ERROR)) {
        					mostraError();
        				} 
        			}
        		}));
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }

	private void mostraError() {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Error");
		alert.setHeaderText(null);
		alert.setContentText("Em temo que el programa no funciona!");

		alert.show();
		
	}
	
}
